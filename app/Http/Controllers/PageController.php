<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{

    public function welcome(){
        return view('pages/welcome');
    }

    public function contact(){
        return view('pages/contact');
    }

    public function services(){
        return view('pages/services');
    }

    public function menu(){
        return view('pages/menu');
    }
}
